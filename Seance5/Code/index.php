<?php
// Mise en place de l'autoload
require_once 'vendor/autoload.php';
use gamepedia\modele as m;
use gamepedia\controleur as c;
use Illuminate\Database\Capsule\Manager as DB;
use Slim\Slim as s;

// On démarre la connexion avec la bd avec eloquent
$db = new DB();
$db->addConnection(parse_ini_file("./src/conf/conf.ini"));
$db->setAsGlobal();
$db->bootEloquent();

// On déclare la variable SLIM pour créer les routes
$app = new s();

// Partie n°1 : Accéder un jeu
// Partie n°6 : retour sur les jeux
$app->get("/api/games/:id",function($id){
  c\ControleurJSON::afficherJeu($id);
})->name("jeux");

// Partie n°2 : accéder à une collection de jeux
// Partie n°3: pagination
// Partie n°4 : lien collection --> ressource
$app->get("/api/games",function(){
  c\ControleurJSON::accederCollectionJeux();
})->name("collection");

// Partie n°5 : commentaires sur les jeux
$app->get("/api/games/:id/comments",function($id){
    c\ControleurJSON::afficherCommentaires($id);
})->name("commentaire");

// Partie n°7 : les personnages d'un jeu
$app->get("/api/games/:id/characters",function($id){
    c\ControleurJSON::afficherCharacters($id);
})->name("characters");

// Partie n°7 : les personnages d'un jeu
$app->get("/api/characters/:id",function($id){
    c\ControleurJSON::afficherCharacterParticulier($id);
})->name("character");

// Partie n°8 : ajouter des commentaires
$app->post("/api/games/:id/comments",function($id){
    c\ControleurJSON::ajouterCommentaire($id);
});

// Partie n°8 : lien vers la ressource d'un commentaire
$app->get("/api/comments/:id",function($id){
    c\ControleurJSON::afficherCommentaireParticulier($id);
})->name("com");

$app->run();
