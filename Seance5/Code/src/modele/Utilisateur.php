<?php

namespace gamepedia\modele;

/**
 * Classe Liste qui permet de modéliser une liste dans la bdd
 */
class Utilisateur extends \Illuminate\Database\Eloquent\Model{
  // Table liste
  protected $table = 'utilisateur';
  // Clé primaire : id
  protected $primaryKey = 'id';
  public $timestamps = false;

  public function commentaires(){
    return $this->hasMany("gamepedia\modele\Commentaire","idUtilisateur");
  }
}
