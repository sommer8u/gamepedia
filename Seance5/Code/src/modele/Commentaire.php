<?php
/**
 * Created by PhpStorm.
 * User: matthieu
 * Date: 28/03/2018
 * Time: 14:16
 */

namespace gamepedia\modele;

/**
 * Classe Liste qui permet de modéliser une liste dans la bdd
 */
class Commentaire extends \Illuminate\Database\Eloquent\Model{
    // Table liste
    protected $table = 'commentaire';
    // Clé primaire : id
    protected $primaryKey = 'idCommentaire';
    public $timestamps = true;


    protected $dates = [
        "created_at",
        "updated_at"
    ];

    public function email_commented(){
        return $this->belongsTo("gamepedia\modele\Utilisateur","idUtilisateur");
    }

    public function game(){
        return $this->belongsTo("gamepedia\modele\Game","idGame");

    }


}
