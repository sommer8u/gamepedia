<?php
namespace gamepedia\controleur;

use gamepedia\modele as m;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Slim\Slim as s;
use Respect\Validation\Validator as v;

class ControleurJSON {
    /**
     * Affichage d'un jeu en particulier
     */
    public static function afficherJeu($id) {
      // On récupère l'instance
      $app = \Slim\Slim::getInstance();
      // On précise le type du contenu
      $app->response->header('Content-Type', 'application/json');
      $platform = [];
      try {
        // On récupère le jeu
        $jeu = m\Game::where("id","=",$id)->firstOrFail();
        // Puis ses plates-formes
        $platform = $jeu->platforms()->get();

        $arr = [];
        // Pour chaque plate-forme on affiche l'id le nom l'alias et une abbréviation
        foreach($platform as $v) {
          $arr[] = [
              "id" => $v["id"],
              "name" => $v["name"],
              "alias" => $v["alias"],
              "abbreviation" => $v["abbreviation"]];
          }
      }catch(ModelNotFoundException $e) {
        // En cas d'échec, on change le code d'erreur
        $app->response->setStatus(404);
        echo json_encode(["msg" => "game $id not found"]);
      }

      // Affichage du jeu avec ses plates-formes
      echo json_encode([
          "game" => $jeu,
          "links" => [
              "comments" => $app->urlFor("commentaire", ["id" => $id]),
              "characters" => $app->urlFor("characters", ["id" => $id])
          ],
          "platforms" => $arr
      ]);
    }

    /**
     * Affichage de l'ensemble des jeux avec pagination
     */
    public static function accederCollectionJeux(){
        // PARTIE n°2 : Accéder à une collection de jeux
        // PARTIE n°3 : pagination
        // PARTIE n°4 : lien collection --> ressource
        $app = s::getInstance();

        // On notifie le type du contenu
        $app->response->headers->set('Content-Type','application/json');

        // On regarde le paramètre
        $page = $app->request->get('page');
        // En fonction de la page on définit les pages précédentes et les pages suivantes
        if($page == null ||$page <= 0){
            $prev = 0;
            $suiv = 1;
            $page = 0;
        }else{
            $prev = $page-1;
            $suiv = $page+1;
        }
        // Puis on affiche les 200 jeux demandés
        $jeux = m\Game::take(200)->skip(200*$page)->get();
        $arr = [];
        foreach ($jeux as $value) {
            $arr[] = ["game"=>["id"=>$value["id"],
                "name"=>$value["name"],
                "alias"=>$value["alias"],
                "deck"=>$value["deck"]],
                "links" => ["self"=>["href"=>$app->urlFor("jeux",["id"=>$value["id"]])]]];
        }
        // Affiche en json des informations
        echo json_encode(["games" => $arr,
            "links" => [
                "prev" => [
                    "href" => $app->urlFor("collection")."?page=".$prev
                ],
                "next"=> [
                    "href" => $app->urlFor("collection")."?page=".$suiv
                ]
            ]]);
    }

    public static function afficherCommentaires($id){
        // PARTIE n°5 : commentaires sur les jeux
        $app = s::getInstance();

        // On notifie le type du contenu
        $app->response->headers->set('Content-Type','application/json');
        // On récupère la liste des commentaires pour un jeu donné
        $commentaires = m\Commentaire::where("idGame","=",$id)->get();

        $aux = [];
        // Pour chaque commentaire on affiche le titre, l'id, le texte, la date de création, le nom et le prénom de l'utilisateur
        foreach ($commentaires as $value) {
            $utilisateur = m\Utilisateur::where("id","=",$value["idUtilisateur"])->first();
            $aux[] = ["id"=>$value["id"],
                "titre"=>$value["titre"],
                "texte"=>$value["contenu"],
                "Date de création" => $value["dateCreation"],
                "Nom de l'utilisateur"=>$utilisateur["nom"],
                "Prénom de l'utilisateur"=>$utilisateur["prenom"]];
        }
        // On affiche ensuite les informations sous la forme d'un json
        echo json_encode(["commentaires"=>$aux]);
    }

    /**
     * Affichage des caractères d'un jeu
     */
    public static function afficherCharacters($id){
        // PARTIE n°7 : les personnages d'un jeu
        $app = s::getInstance();

        // On notifie le type du contenu
        $app->response->headers->set('Content-Type','application/json');
        // On récupère le jeu
        $games = m\Game::where("id","=",$id)->first();

        $aux = [];
        // on récupère les personnages de ce jeu
        $character = $games->characters()->get();
        foreach ($character as $value) {
          $aux[] = ["character"=>["id"=>$value["id"],
            "name"=>$value["name"]],
            "links"=>[
              "self" => [
                "href" => $app->urlFor("character",["id"=>$value["id"]])
              ]
              ]];
        }
        // Affichage en json
        echo json_encode(["characters"=>$aux]);
    }

    /**
     * Affichage d'un personnage en particulier
     */
    public static function afficherCharacterParticulier($id){
        // PARTIE n°7 : un personnage en particulier
        $app = s::getInstance();

        // On notifie le type du contenu
        $app->response->headers->set('Content-Type','application/json');
        // Récupération du personnage
        $character = m\Character::find($id);
        // Affichage
        echo json_encode([$character->toArray()]);
    }

    /**
     * Ajout d'un commentaire
     */
    public static function ajouterCommentaire($id){
        // PARTIE n°8 : ajouter des commentaires
        $app = s::getInstance();

        // On notifie le type du contenu
        $app->response->headers->set('Content-Type','application/json');
        // Message d'erreur
        $err;
        try {
          // Création du nouveau commentaire
          $commentaire = new m\Commentaire();
          // On utilise les valeurs du post
          $commentaire->titre = filter_var($app->request->post("titre"),FILTER_SANITIZE_STRING);
          $commentaire->contenu = filter_var($app->request->post("contenu"),FILTER_SANITIZE_STRING);
          $date = $app->request->post("dateCreation");
          list($annee,$mois,$jour) = explode('-',$date);
          if(checkdate($mois,$jour,$annee)){
            $commentaire->dateCreation = $date;
          }else{
            $err = ["msg" => "date not valid"];
            throw new ModelNotFoundException();
          }
          $iduser = filter_var($app->request->post("idUtilisateur"),FILTER_SANITIZE_NUMBER_INT);
          // On regarde si l'utilisateur existe et si iduser est bien un nombre
          if(m\Utilisateur::find($iduser) != null && v::numeric()->validate($iduser)){
            $commentaire->idUtilisateur = $iduser;
          }else{
            $err = ["msg" => "user $iduser not found"];
            throw new ModelNotFoundException();
          }
          if($id > 0){
            $commentaire->idGame = $id;
          }else{
            $err = ["msg" => "game $id not found"];
            throw new ModelNotFoundException();
          }

          // On sauvegarde l'item
          $commentaire->save();
          // On place le code de retour de la requête à 201
          $app->response->setStatus(201);
          // On place dans le header la location de la ressource initiale
          $app->response->headers->set('location',$app->urlFor("com",["id"=>$commentaire->idCommentaire]));
          // On encode ensuite le commentaire pour l'afficher
          echo json_encode(m\Commentaire::find($commentaire->idCommentaire));
        }catch(ModelNotFoundException $e) {
          $app->response->setStatus(400);
          echo json_encode($err);
        }
    }

    /**
     * Affichage d'un commentaire en particulier
     */
    public static function afficherCommentaireParticulier($id){
      // PARTIE n°8 : affichage d'un commentaire en particulier
      $app = s::getInstance();

      // On notifie le type du contenu
      $app->response->headers->set('Content-Type','application/json');

      $com = m\Commentaire::find($id);

      echo json_encode([$com->toArray()]);
    }
}
?>
