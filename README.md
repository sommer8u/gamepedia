# **Applications et Base de Données** #

***

### **Collaborateurs :** ###

```
BAZART Méderic
CESARO Alexis
SIES Reynault
SOMMER Matthieu
```

***

## **Présentation Séance 2** ##

* 1) Schéma SQL

```
Catégorie (id, nom, descr)
Annonce (id, titre, date, texte)
Photo (id, file, date, taille_octet, #idAnnonce)
AnnonceAppartientCategorie (#idAnnonce, #idCategorie)
```

* 2) Méthodes pour faire l'association

```php
<?php
Dans la classe Photo :
belongsTo('gamepedia\modele\Annonce','id_photo');

Dans la classe Annonce :
hasMany('gamepedia\modele\Photo', 'id_annonce');
belongsToMany('gamepedia\modele\Categorie', 'annonceappartientcategorie', 'id_annonce', 'id_categorie');

Dans la classe Categorie :
belongsToMany('gamepedia\modele\Annonce', 'annonceappartientcategorie', 'id_categorie', 'id_annonce');
?>
```

* 3) Requêtes

_1° Les photos de l'annonce 22 :_

```php
<?php
$annonce = m\Annonce::find(22);
$photos = $annonce->photos()->get();

foreach ($photos as $value) {
    echo "id : ".$value["id"].", fichier : ".$value["file"].", taille : ".$value["taille_octet"]." octet(s)</br>";
}
?>
```

_2° Les photos de l'annonce 22 dont la taille en octets est > 100000_

```php
<?php
$annonce = m\Annonce::find(22);
$photos = $annonce->photos()->where("taille_octet",">","100000")->get();

foreach ($photos as $value) {
    echo "id : ".$value["id"].", fichier : ".$value["file"].", taille : ".$value["taille_octet"]." octet(s)</br>";
}
?>
```

_3° Les annonces possédant plus de 3 photos_
```php
<?php
$annonce = m\Annonce::has('photos','>',3)->get();

foreach ($annonce as $value) {
    echo "id : ".$value["id"].", Titre : ".$value["titre"].", date : ".$value["date"].", texte : ".$value["texte"]."</br>";
}
?>
```
_4° Les annonces possédant des photos dont la taille est > 100000_

```php
<?php
$annonce = m\Annonce::get();

foreach ($annonce as $value) {
    $annonceTaille = $value->photos()->where("taille_octet",">","100000")->get();
    foreach ($annonceTaille as $valuePhotos) {
        echo "id : ".$valuePhotos["id_photo"].", fichier : ".$valuePhotos["file"].", taille : ".$valuePhotos["taille_octet"]." octet(s)</br>";
    }
}
?>
```

* 4) Ajouter une photo à l'annonce 22

```php
<?php
$photo = new m\Photo();
$photo->id_photo = "99999";
$photo->file = "file";
$photo->date = "13/03/2018";
$photo->taille_octet = "1000";
$photo->id_annonce = "22";

$annonce = m\Annonce::find(22);
$annonce->photos()->save($photo);
?>
```

* 5) Ajouter l'annonce 22 aux catégories 42 et 73

```php
<?php
$annonce = m\Annonce::find(22);
$annonce->categories()->attach([42,73]);
?>
```

***

## **Présentation Séance 3** ##

#### **Partie 1** ####

* 1) Mesure du temps d'éxécution

```php
<?php
$time_start = microtime(true);

//Requete

$time_end = microtime(true);
$time = $time_end - $time_start;

echo "La requête s'est éffectué en $time secondes\n";
?>
```
* 2) Principe de l'index

  L'indexation permet :

• D'accéder à ses données plus rapidement
• De définir le degré d’unicité d’une colonne donnée
(chaque champ doit-il être unique ? les doublons sont-ils autorisés ?)


#### **Partie 2** ####

* 1) Structure des logs des requêtes avec Eloquent

On peut voir que les logs affichent les requêtes dans l’ordre telles qu’elles sont exécutées par Eloquent.
À gauche se trouve les requêtes et à droite se trouve les valeurs des paramètres utilisés.

Laravel propose une méthode pour désactiver les logs :

DB::connection()->disableQueryLog();

Pour recevoir une collection des logs :

```php
<?php
$queries = DB::getQueryLog();
?>
```

* 2) Problème des N+1 query

L’utilisation d’un ORM comme Eloquent pour exécuter une requête comme celle présentée ci-dessus pose problème. En effet, l’ORM lance une requête pour obtenir toutes les plateformes puis pour chacune d’entre elles, une requête permettant de recevoir le producteur correspondant ce qui fait N+1 requêtes.
L’ORM n’est donc pas efficace dans ce cas puisque cette requête peut se réaliser en une requête SQL, en réalisant la jointure entre la table producteur et plateforme.
Elle peut également être réalisée en deux requêtes, la première permet de récupérer toutes les plateformes et la deuxième permet de recevoir les producteurs grâce à un IN dans lequel on place tous les ID.
On peut forcer l’ORM à utiliser cette deuxième technique en utilisant la méthode WITH en précisant ‘PRODUCTEUR’.

***

## **Présentation Séance 4** ##

* 1) Comment installer Faker ?

```php
<?php
composer require fzaninotto/faker
?>
```

* 2) Exemple de code pour générer une adresse américaine

```php
<?php
$faker = Faker\Factory::create('en_US');
$address = $faker->address;
?>
```

* 3) Format de date du type DateTime
: "2017/02/16 (16:15)"

```php
<?php
$date = new DateTime('2017/02/16 16:15');
echo $date->format('Y/m/d (H:i)');
?>
```

***

## **Présentation Séance 5 et 6** ##

* 1) Les résultats de la fonction json_encode()
  - Un tableau json :

```php
<?php
  echo "Tableau non-associatif sous forme de tableau : ",
  json_encode($c);
?>
```

  - Un objet json :

```php
<?php
  echo "Tableau non-associatif sous forme d'objet : ",
  json_encode($c, JSON_FORCE_OBJECT);
?>
```

* 2) Slim v2.0 : Accèder aux données transmises dans la
requête
  - Les données dans l'url :
    - GET

      Utiliser la methode get() de l'application Slim pour récupérer la fonction callback liée à la ressource URI qui est requise pour la methode HTTP GET.

    - POST

      Utiliser la methode post() de l'application Slim pour récupérer la fonction callback liée à la ressource URI qui est requise pour la methode HTTP POST.

  - Les données dans le corps de la requête

```php
<?php  
  $app = new \Slim\Slim();  
  $app->get('/books/:id', function ($id) {  
  //Show book identified by $id  
});
?>
```
Dans cet exemple, une requête HTTP GET pour “/books/1” invoquera la fonction callback associée, passant "1" en paramètre de celle-ci.
Le premier argument de la méthode get() de l'application Slim est la ressource URI. Le dernier argument est n'importe quoi retournant vrai pour is_callable(). Habituellement, le dernier argument est une fonction anonyme.

```php
<?php  
  $app = new \Slim\Slim();  
  $app->post('/books', function () {  
    //Create book  
  });  
?>
```
Dans cet exemple, une requête HTTP POST pour “/books” invoquera la fonction callback.
Le premier argument de la méthode post() de l'application Slim est la ressource URI. Le dernier argument est n'importe quoi retournant vrai pour is_callable(). Habituellement, le dernier argument est une fonction anonyme.

On peut utiliser :

```php
<?php
  $body = $app->request->getBody();
?>
```

* 3) Comment insérer :

  - Un code de retour-réponse :
```php
<?php
  $app->response->setStatus(400);
?>
```
  _Pour pouvoir set un code réponse_

```php
<?php
  $status = $app->response->getStatus();
?>
```
    _Pour pouvoir récupérer le status_

  - Un header :
```php
<?php
  $app = new \Slim\Slim();
  $app->response->headers->set('Content-Type', 'application/json');
?>
```
  _Pour pouvoir set un header_
```php
<?php
  $contentType = $app->response->headers->get('Content-Type');
?>
```
  _Pour pouvoir récupérer le status_

***
