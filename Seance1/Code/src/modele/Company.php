<?php

namespace gamepedia\modele;

/**
 * Classe Liste qui permet de modéliser une liste dans la bdd
 */
class Company extends \Illuminate\Database\Eloquent\Model{
  // Table liste
  protected $table = 'company';
  // Clé primaire : id
  protected $primaryKey = 'id';
  public $timestamps = false;

  public function platforms(){
    return $this->hasMany('gamepedia\modele\Platform','id');
  }

}
