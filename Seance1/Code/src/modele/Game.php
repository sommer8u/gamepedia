<?php

namespace gamepedia\modele;
/**
 * Classe Item qui correspond à la table Item de la base de données
 */
class Game extends \Illuminate\Database\Eloquent\Model{
  // Table item
  protected $table = 'game';
  // Clé primaire : id
  protected $primaryKey = 'id';
  public $timestamps = false;
}
