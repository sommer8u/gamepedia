<?php
/* PARTIE N°2 */

// Question n°4

// Mise en place de l'autoload
require_once 'vendor/autoload.php';

// Utilisation d'eloquent et des modèles
use Illuminate\Database\Capsule\Manager as DB;
use gamepedia\modele as m;

// On démarre la connexion avec la bd avec eloquent
$db = new DB();
$db->addConnection(parse_ini_file("./src/conf/conf.ini"));
$db->setAsGlobal();
$db->bootEloquent();

if(!isset($_GET['page'])) {
    /*if(isset($_GET['next'])) {
        $a = $_GET['page'] + 1;
    }
    if(isset($_GET['previous'])) {
        $a = $_GET['page'] - 1;
    }*/
    $_GET['page'] = 1;
}
$html = "<a style='font-size: 40px' href='./question5.php?page=".($_GET['page']-1)."'>Previous</a> <a style='font-size: 40px' href='./question5.php?page=".($_GET['page']+1)."'>Next</a>";
if($_GET['page']==1){
  $html =  "<a style='font-size: 40px' href='./question5.php?page=".($_GET['page']+1)."'>Next</a>";
}

print $html . "<br>" . "<br>";

$jeux = m\Game::where("id",">=","0")
    ->take(500)
    ->skip(($_GET['page'] - 1) * 500)
    ->get();

foreach ($jeux as $value) {
    echo $value["name"]."</br>";
}
