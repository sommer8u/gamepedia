<?php
/* PARTIE N°2 */

// Question n°4

// Mise en place de l'autoload
require_once 'vendor/autoload.php';

// Utilisation d'eloquent et des modèles
use Illuminate\Database\Capsule\Manager as DB;
use gamepedia\modele as m;

// On démarre la connexion avec la bd avec eloquent
$db = new DB();
$db->addConnection(parse_ini_file("./src/conf/conf.ini"));
$db->setAsGlobal();
$db->bootEloquent();

$jeux = m\Game::where("id",">=","21173")->take(442)->get();

foreach ($jeux as $value) {
  echo $value["name"]."</br>";
}
