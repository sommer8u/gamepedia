<?php

/* PARTIE N°2 */

// Mise en place de l'autoload
require_once 'vendor/autoload.php';

// Utilisation d'eloquent et des modèles
use Illuminate\Database\Capsule\Manager as DB;
use gamepedia\modele as m;

// On démarre la connexion avec la bd avec eloquent
$db = new DB();
$db->addConnection(parse_ini_file("./src/conf/conf.ini"));
$db->setAsGlobal();
$db->bootEloquent();

// Première requête

// Lister les commentaires d'un utilisateur donné, afficher la date du commentaire de façon
// lisible, ordonnés par date décroissante
$commentaires = m\Commentaire::whereHas('email_commented', function($user){
  $user->where("email","=",$_GET["email"]);
})->orderBy("dateCreation", "DESC")->get();

echo "<h1>Utilisateur : ".$_GET["email"]."</h1>";
foreach ($commentaires as $value) {
  echo "--------------------------------------------</br>";
  echo "<h2>Titre : ".$value["titre"]."</h2>";
  echo "<h3>Contenu : ".$value["contenu"]."</h3>";
  echo "<h4>Date de création : ".$value["dateCreation"]."</h4>";
}
