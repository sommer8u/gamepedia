<?php
/**
 * Created by PhpStorm.
 * User: matthieu
 * Date: 28/03/2018
 * Time: 14:39
 */


// Mise en place de l'autoload
require_once 'vendor/autoload.php';

// Utilisation d'eloquent et des modèles
use Illuminate\Database\Capsule\Manager as DB;
use gamepedia\modele as m;

// On démarre la connexion avec la bd avec eloquent
$db = new DB();
$db->addConnection(parse_ini_file("/Applications/MAMP/htdocs/gamepedia/Seance4/TP/Code/src/conf/conf.ini"));
$db->setAsGlobal();
$db->bootEloquent();



$faker = Faker\Factory::create('fr_FR');

for ($i=0; $i < 25000; $i++) {
    $d = new DateTime();
    $utilisateur = new m\Utilisateur();
    $utilisateur->email = $faker->email;
    $utilisateur->nom = $faker->lastName;
    $utilisateur->prenom = $faker->firstName;
    $utilisateur->adresse = $faker->address;
    $utilisateur->numTel = $faker->phoneNumber;
    $utilisateur->dateNaiss = $faker->dateTimeBetween($startDate = '-80 years', $endDate = '-18 years', $timezone = null)->format('Y-m-d');
    echo $utilisateur . "<br>";
    $utilisateur->save();
}

for ($i=0; $i < 250000; $i++) {
    $d = new DateTime();
    $commentaire = new m\Commentaire();
    $commentaire->titre = $faker->email;
    $commentaire->contenu = $faker->lastName;
    $commentaire->dateCreation = $faker->firstName;
    $commentaire->emailUtilisateur = $faker->address;
    echo $commentaire . "<br>";
    $commentaire->save();
}

