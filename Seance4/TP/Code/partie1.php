<?php

/* PREPARATION SEANCE N°3 */

/* PARTIE N°1 */

// Mise en place de l'autoload
require_once 'vendor/autoload.php';

// Utilisation d'eloquent et des modèles
use Illuminate\Database\Capsule\Manager as DB;
use gamepedia\modele as m;

// On démarre la connexion avec la bd avec eloquent
$db = new DB();
$db->addConnection(parse_ini_file("./src/conf/conf.ini"));
$db->setAsGlobal();
$db->bootEloquent();

// Création des deux utilisateurs

$d = new DateTime();

$user1 = new m\Utilisateur();
$user1->email = "fake1@mail.com";
$user1->nom = "FAKER";
$user1->prenom = "test1";
$user1->adresse = "20 rue du palmier";
$user1->numTel = "0602020202";
$user1->dateNaiss = $d->setDate("1998","12","28");
$user1->save();

$user2 = new m\Utilisateur();
$user2->email = "fake2@mail.com";
$user2->nom = "FAKER";
$user2->prenom = "test2";
$user2->adresse = "20 rue des étoiles";
$user2->numTel = "0602021202";
$user2->dateNaiss = $d->setDate("1998","12","28");
$user2->save();

// Création des trois commentaires

$commentaire1 = new m\Commentaire();
$commentaire1->titre = "Premier commentaire";
$commentaire1->contenu = "Premier commentaire";
$commentaire1->dateCreation = $d->setDate("1998","12","28");
$commentaire1->idUtilisateur = $user1->id;
$commentaire1->idGame = 12342;
$commentaire1->save();

$commentaire2 = new m\Commentaire();
$commentaire2->titre = "Deuxième commentaire";
$commentaire2->contenu = "Deuxième commentaire";
$commentaire2->dateCreation = $d->setDate("1998","12","28");
$commentaire2->idUtilisateur = $user1->id;
$commentaire2->idGame = 12342;
$commentaire2->save();

$commentaire3 = new m\Commentaire();
$commentaire3->titre = "Troisième commentaire";
$commentaire3->contenu = "Troisième commentaire";
$commentaire3->dateCreation =$d->setDate("1998","12","28");
$commentaire3->idUtilisateur = $user1->id;
$commentaire3->idGame = 12342;
$commentaire3->save();

$commentaire4 = new m\Commentaire();
$commentaire4->titre = "Quatrième commentaire";
$commentaire4->contenu = "Quatrième commentaire";
$commentaire4->dateCreation = $d->setDate("1998","12","28");
$commentaire4->idUtilisateur = $user2->id;
$commentaire4->idGame = 12342;
$commentaire4->save();

$commentaire5 = new m\Commentaire();
$commentaire5->titre = "Cinquième commentaire";
$commentaire5->contenu = "Cinquième commentaire";
$commentaire5->dateCreation = $d->setDate("1998","12","28");
$commentaire5->idUtilisateur = $user2->id;
$commentaire5->idGame = 12342;
$commentaire5->save();

$commentaire6 = new m\Commentaire();
$commentaire6->titre = "Sixième commentaire";
$commentaire6->contenu = "Sixième commentaire";
$commentaire6->dateCreation = $d->setDate("1998","12","28");
$commentaire6->idUtilisateur = $user2->id;
$commentaire6->idGame = 12342;
$commentaire6->save();
?>
