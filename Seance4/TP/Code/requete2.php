<?php

/* PARTIE N°2 */

// Mise en place de l'autoload
require_once 'vendor/autoload.php';

// Utilisation d'eloquent et des modèles
use Illuminate\Database\Capsule\Manager as DB;
use gamepedia\modele as m;

// On démarre la connexion avec la bd avec eloquent
$db = new DB();
$db->addConnection(parse_ini_file("./src/conf/conf.ini"));
$db->setAsGlobal();
$db->bootEloquent();

// Deuxième requête

// Lister les utilisateurs ayant posté plus de 5 commentaires.
$utilisateurs = m\Utilisateur::has('commentaires','>',5)->get();

foreach ($utilisateurs as $value) {
  echo "<h2>Email : ".$value["email"]."</h2>";
  echo "<h3>Nom : ".$value["nom"]."</h3>";
  echo "<h3>Prenom : ".$value["prenom"]."</h3>";
  echo "<h3>Numéro de téléphone : ".$value["numTel"]."</h3>";
  echo "<h3>Date de naissance : ".$value["dateNaiss"]."</h3>";
  echo "<h3>Adresse : ".$value["adresse"]."</h3>";
  echo "--------------------------------------------</br>";
}
