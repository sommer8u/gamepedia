<?php

/* PREPARATION SEANCE N°3 */

/* PARTIE N°1 */

// Mise en place de l'autoload
require_once 'vendor/autoload.php';

// Utilisation d'eloquent et des modèles
use Illuminate\Database\Capsule\Manager as DB;
use gamepedia\modele as m;

// On démarre la connexion avec la bd avec eloquent
$db = new DB();
$db->addConnection(parse_ini_file("./src/conf/conf.ini"));
$db->setAsGlobal();
$db->bootEloquent();

// question n°1
/*
$time_start = microtime(true);

m\Game::get();

$time_end = microtime(true);
$time = $time_end - $time_start;

echo "La première requête s'est effectuée en $time secondes</br>";
*/
// question n°2

$time_start = microtime(true);

m\Game::where("name","like","%Mario%")->get();

$time_end = microtime(true);
$time = $time_end - $time_start;

echo "La deuxième requête s'est effectuée en $time secondes</br></br>";

// question n°3

$time_start = microtime(true);

$jeux = m\Game::where("name","like","Mario%")->get();

foreach ($jeux as $value) {
  $value->characters()->get();
}

$time_end = microtime(true);
$time = $time_end - $time_start;

echo "La troisième requête s'est effectuée en $time secondes</br></br>";

// question n°4

$time_start = microtime(true);

m\Game::where("name","like","Mario%")
      ->whereHas("ratings",function($d){
        $d->where("name","like","%3+%");
      })->get();

$time_end = microtime(true);
$time = $time_end - $time_start;

echo "La quatrième requête s'est effectuée en $time secondes</br></br>";

// Première requête : 1.71123454894213 secondes

// Deuxièmre requête : 0.91442394256592 secondes

// Troisième requête : 8.307344913483 secondes

// Quatrième requête : 2.5664210319519 secondes

// Plus la requête est couteuse, plus le temps d'exécution est long
// Une même requête exéctuée plusieures fois n'a pas toujours le même temps d'exécution

// Cache de requêtes mysql : comparer le temps d'exécution d'une requête coûteuse entre la 1ère
// exécution et les exécutions suivantes.

// Première exécution de la deuxième requête (avec un index): 0.78681397438049 secondes

// Deuxième exécution de la deuxième requête (avec un index): 0.7762439250946 secondes

// Troisième exécution de la deuxième requête (avec un index): 0.76189398765564 secondes

// Gain de performance : 2.5%

  // On remarque que le temps d'exécution diminiue légerement
  // Le cache conserve le résultat en mémoire

// Etudier la requête : "lister les jeux dont le nom débute par '<valeur>' "
// mesurer son temps d'exécution avec 3 valeurs différentes

// Test 1

$time_start = microtime(true);

m\Game::where("name","like","Peach%")->get();

$time_end = microtime(true);
$time = $time_end - $time_start;

echo "La requête avec Peach s'est effectuée en $time secondes</br></br>";

// Test 2

$time_start = microtime(true);

m\Game::where("name","like","Space%")->get();

$time_end = microtime(true);
$time = $time_end - $time_start;

echo "La requête avec Space s'est effectuée en $time secondes</br></br>";

// Test 3

$time_start = microtime(true);

m\Game::where("name","like","Test%")->get();

$time_end = microtime(true);
$time = $time_end - $time_start;

echo "La requête avec Test s'est effectuée en $time secondes</br></br>";

// Sans index :

// Requête avec Peach : 0.9224100112915 secondes

// Requête avec Space : 0.95016002655029 secondes

// Requête avec Test : 0.87647008895874  secondes

// une moyenne de 0.916 secondes

// On remarque que les temps sont similaires, l'exécution dure presque une seconde sans index

// Avec index :

// Requête avec Peach : 0.0031428337097168 secondes

// Requête avec Space : 0.045466899871826  secondes

// Requête avec Test : 0.0075390338897705  secondes

// une moyenne de 0.018 secondes

// Ce qui fait un gain de : 0.916 - 0.018 / 0.916 = 98%
// L'ajout d'un index sur la colonne name de la table game permet de
// réduire considérablement le temps d'exécution des requêtes

// En effet, L'index permet une recherche plus efficace sur les noms

// Etudier la requête : lister les jeux dont le nom contient '<valeur>'. Pouvezvous
// expliquer le résultat ?

// Test 1

$time_start = microtime(true);

m\Game::where("name","like","%Peach%")->get();

$time_end = microtime(true);
$time = $time_end - $time_start;

echo "La requête avec Peach s'est effectuée en $time secondes</br></br>";

// Test 2

$time_start = microtime(true);

m\Game::where("name","like","%Space%")->get();

$time_end = microtime(true);
$time = $time_end - $time_start;

echo "La requête avec Space s'est effectuée en $time secondes</br></br>";

// Test 3

$time_start = microtime(true);

m\Game::where("name","like","%Test%")->get();

$time_end = microtime(true);
$time = $time_end - $time_start;

echo "La requête avec Test s'est effectuée en $time secondes</br></br>";


// Requête avec Peach : 0.71962094306946  secondes

// Requête avec Space : 0.75482487678528   secondes

// Requête avec Test : 0.71639680862427   secondes

// En moyenne = 0.729 secondes

// On remarque que l'index sur la colonne name n'impact pas l'exécution de ces requêtes
// En effet, L'index ne peut pas être utilisé pour filtrer les noms puisqu'on cherche tous les noms qui contiennent une
// valeur et qui commencent avec n'importe quelle valeur.

// Etudiez sur le même principe la requête "Liste des compagnies d'un pays(location_country)" :
// évaluez le gain de performance amené par un index. Que pensez-vous du résultat ?

// Test 1

$time_start = microtime(true);

m\Company::where("location_country","like","United%")->get();

$time_end = microtime(true);
$time = $time_end - $time_start;

echo "La requête avec United s'est effectuée en $time secondes</br></br>";

// Test 2

$time_start = microtime(true);

m\Company::where("location_country","like","S%")->get();

$time_end = microtime(true);
$time = $time_end - $time_start;

echo "La requête avec S s'est effectuée en $time secondes</br></br>";

// Test 3

$time_start = microtime(true);

m\Company::where("location_country","like","F%")->get();

$time_end = microtime(true);
$time = $time_end - $time_start;

echo "La requête avec F s'est effectuée en $time secondes</br></br>";

// Résultats sans index

// Requête avec United : 0.16375088691711 secondes
// Requête avec S : 0.050040006637573 secondes
// Requête avec F : 0.039630889892578 secondes

// Moyenne = 0.08 secondes

// Ajout d'un index sur la colonne location_country de la table company

// Résultats avec index

// Requête avec United : 0.14833903312683 secondes
// Requête avec S : 0.030977964401245  secondes
// Requête avec F : 0.019124031066895  secondes

// Moyenne = 0.063 secondes

// Gain = 0.08 - 0.063 / 0.08 = 21.25 %

// On remarque que les requêtes ne sont pas beaucoup plus rapides : Il n'y a pas assez de valeurs pour que l'index soit vraiment utile
 ?>
