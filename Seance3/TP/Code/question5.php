<?php
/**
 * Created by PhpStorm.
 * User: matthieu
 * Date: 16/03/2018
 * Time: 10:38
 */

require_once 'vendor/autoload.php';

use Illuminate\Database\Capsule\Manager as DB;
use gamepedia\modele as m;


$db = new DB();
$db->addConnection(parse_ini_file("./src/conf/conf.ini"));
$db->setAsGlobal();
$db->bootEloquent();
DB::connection()->enableQueryLog();

$company = m\Company::where("name","like","%Sony%")->get();

foreach ($company as $value) {
    $jeux = $value->games_developed()->get();
}

$queries5 = DB::getQueryLog();

echo "<br>Question 5:<br>";
foreach ($queries5 as $value5) {
    echo "Query: " . $value5['query'] . "<br>";
    foreach ($value5['bindings'] as $value5B) {
        echo "Bindings: " . $value5B . "<br>";
    }
    echo "Time: " . $value5['time'] . "<br><br>";
}