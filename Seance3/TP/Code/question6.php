<?php
/**
 * Created by PhpStorm.
 * User: matthieu
 * Date: 16/03/2018
 * Time: 10:38
 */

require_once 'vendor/autoload.php';

use Illuminate\Database\Capsule\Manager as DB;
use gamepedia\modele as m;


$db = new DB();
$db->addConnection(parse_ini_file("./src/conf/conf.ini"));
$db->setAsGlobal();
$db->bootEloquent();
DB::connection()->enableQueryLog();

// Utilisation des chargements liés

// Reprogrammez la requête "afficher le nom des personnages des jeux dont le nom (de jeu) contient
// 'Mario'" en utilisant un chargement lié.

m\Character::with(["firstGame" => function($a){
  $a->where('name', 'like', '%Mario%');
}])->get();

$queries2 = DB::getQueryLog();

echo "<br>Question 6:<br>";
foreach ($queries2 as $value2) {
    echo "Query: " . $value2['query'] . "<br>";
    foreach ($value2['bindings'] as $value2B) {
        echo "Bindings: " . $value2B . "<br>";
    }
    echo "Time: " . $value2['time'] . "<br><br>";
}

// Seulement deux requêtes sont exécutées. La deuxième requête utilise un IN dans lequel elle bind les ids des personnages.
