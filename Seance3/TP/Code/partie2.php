<?php

/* PREPARATION SEANCE N°3 */

/* PARTIE N°2 */

// Mise en place de l'autoload
require_once 'vendor/autoload.php';

// Utilisation d'eloquent et des modèles
use Illuminate\Database\Capsule\Manager as DB;
use gamepedia\modele as m;

// On démarre la connexion avec la bd avec eloquent
$db = new DB();
$db->addConnection(parse_ini_file("./src/conf/conf.ini"));
$db->setAsGlobal();
$db->bootEloquent();
DB::connection()->enableQueryLog();

// question n°1
include("question1.php");

// question n°2
include("question2.php");

// question n°3
include("question3.php");

// question n°4
include("question4.php");

// question n°5
include("question5.php");

// question n°6
include("question6.php");

// question n°7
include("question7.php");
