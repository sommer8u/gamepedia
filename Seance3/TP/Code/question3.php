<?php
/**
 * Created by PhpStorm.
 * User: matthieu
 * Date: 16/03/2018
 * Time: 10:38
 */

require_once 'vendor/autoload.php';

use Illuminate\Database\Capsule\Manager as DB;
use gamepedia\modele as m;


$db = new DB();
$db->addConnection(parse_ini_file("./src/conf/conf.ini"));
$db->setAsGlobal();
$db->bootEloquent();
DB::connection()->enableQueryLog();

m\Character::whereHas("firstGame", function($a){$a->where('name', 'like', '%Mario%');})->get();

$queries3 = DB::getQueryLog();

echo "<br>Question 3:<br>";
foreach ($queries3 as $value3) {
    echo "Query: " . $value3['query'] . "<br>";
    foreach ($value3['bindings'] as $value3B) {
        echo "Bindings: " . $value3B . "<br>";
    }
    echo "Time: " . $value3['time'] . "<br><br>";
}