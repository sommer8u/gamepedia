<?php
/**
 * Created by PhpStorm.
 * User: matthieu
 * Date: 16/03/2018
 * Time: 10:38
 */

require_once 'vendor/autoload.php';

use Illuminate\Database\Capsule\Manager as DB;
use gamepedia\modele as m;


$db = new DB();
$db->addConnection(parse_ini_file("./src/conf/conf.ini"));
$db->setAsGlobal();
$db->bootEloquent();
DB::connection()->enableQueryLog();

// Utilisation des chargements liés

// Reprogrammez la requête "jeux développés par une compagnie dont le nom contient 'Sony'".

m\Game::with(["games_developers" => function($a){
  $a->where('name', 'like', '%Sony%');
}])->get();

$queries2 = DB::getQueryLog();

echo "<br>Question 7:<br>";
foreach ($queries2 as $value2) {
    echo "Query: " . $value2['query'] . "<br>";
    foreach ($value2['bindings'] as $value2B) {
        echo "Bindings: " . $value2B . "<br>";
    }
    echo "Time: " . $value2['time'] . "<br><br>";
}

// Deux requête : La première pour récupérer l'ensemble des jeux et la deuxième pour récupérer
// les jeux développés par une compagnie dont le nom contient 'Sony' en utilisant un IN(?,?,? ...)  ? les ids des jeux
