<?php
/**
 * Created by PhpStorm.
 * User: matthieu
 * Date: 16/03/2018
 * Time: 10:38
 */

require_once 'vendor/autoload.php';

use Illuminate\Database\Capsule\Manager as DB;
use gamepedia\modele as m;

$db = new DB();
$db->addConnection(parse_ini_file("./src/conf/conf.ini"));
$db->setAsGlobal();
$db->bootEloquent();
DB::connection()->enableQueryLog();

m\Game::where("name","like","%Mario%")->get();

$queries1 = DB::getQueryLog();
echo "Question 1: <br>";
foreach ($queries1 as $value1) {
    echo "Query: " . $value1['query'] . "<br>";
    foreach ($value1['bindings'] as $value1B) {
        echo "Bindings: " . $value1B . "<br>";
    }
    echo "Time: " . $value1['time'] . "<br><br>";
}