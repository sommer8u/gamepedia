<?php
/**
 * Created by PhpStorm.
 * User: matthieu
 * Date: 16/03/2018
 * Time: 10:38
 */

require_once 'vendor/autoload.php';

use Illuminate\Database\Capsule\Manager as DB;
use gamepedia\modele as m;


$db = new DB();
$db->addConnection(parse_ini_file("./src/conf/conf.ini"));
$db->setAsGlobal();
$db->bootEloquent();
DB::connection()->enableQueryLog();

$games = m\Game::where("name","like","Mario%")->get();

foreach ($games as $value) {
    $perso = $value->characters()->get();
}

$queries4 = DB::getQueryLog();

echo "<br>Question 4:<br>";
foreach ($queries4 as $value4) {
    echo "Query: " . $value4['query'] . "<br>";
    foreach ($value4['bindings'] as $value4B) {
        echo "Bindings: " . $value4B . "<br>";
    }
    echo "Time: " . $value4['time'] . "<br><br>";
}

